﻿using NServiceBus;

namespace MSMQHelper.Tests
{
    public class SimpleMessage : IMessage
    {
        public string MyProperty { get; set; }
    }

    public class SimpleType
    {
        public string MyProperty { get; set; }
    }

    public class MessageWithNoDefaultConstructor : IMessage
    {
        public MessageWithNoDefaultConstructor(string myProperty)
        {
            MyProperty = myProperty;
        }

        public string MyProperty { get; set; }
    }

    public class MessageWithEncryptedString : IMessage
    {
        public WireEncryptedString MyProperty { get; set; }
    }
}
