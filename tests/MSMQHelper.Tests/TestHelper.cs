﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Caliburn.Micro;

namespace MSMQHelper.Tests
{
    public static class TestHelper
    {
        static TestHelper()
        {
            IoC.BuildUp = x => { };
        }

        public static ResultCompletionEventArgs BlockingExecute(this IResult result, ActionExecutionContext context = null)
        {
            var wait = new AutoResetEvent(false);

            ResultCompletionEventArgs args = null;
            result.Completed += (sender, eventArgs) =>
                                    {
                                        args = eventArgs;
                                        wait.Set();
                                    };

            result.Execute(context);

            wait.WaitOne();

            return args;
        }
    }
}
