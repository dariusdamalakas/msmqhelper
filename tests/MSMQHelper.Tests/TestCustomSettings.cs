﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSMQHelper.Models;
using NUnit.Framework;

namespace MSMQHelper.Tests
{
    [TestFixture]
    public class TestCustomSettings
    {
        [Test]
        public void WhenSerializeUserSettingsThenSerializeSuccessfully()
        {
            var settings = new UserSettings()
                               {MessageDefinition = new MessageDefinition(), QueueDefinition = new QueueDefinition()};
            var xml = Serializer.Serialize(settings);
            Assert.AreEqual("<UserSettings xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n  <MessageDefinition>\r\n    <IsRecoverable>false</IsRecoverable>\r\n  </MessageDefinition>\r\n  <QueueDefinition />\r\n</UserSettings>", xml);
        }
    }
}
