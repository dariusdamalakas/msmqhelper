﻿using Autofac;
using MSMQHelper.Models;
using MSMQHelper.Services;

namespace MSMQHelper
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MSMQService>().As<IMSMQService>();
            builder.RegisterInstance(CustomSettings.Default.UserSettings.QueueDefinition).As<QueueDefinition>();
            builder.RegisterInstance(CustomSettings.Default.UserSettings.MessageDefinition).As<MessageDefinition>();
            builder.RegisterInstance(CustomSettings.Default.UserSettings).As<UserSettings>();
        }
    }
}
