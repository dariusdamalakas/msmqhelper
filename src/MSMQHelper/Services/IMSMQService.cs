using MSMQHelper.Models;

namespace MSMQHelper.Services
{
    public interface IMSMQService
    {
        bool SendMessage(QueueDefinition queueDefinition, MessageDefinition messageDefinition);
    }
}