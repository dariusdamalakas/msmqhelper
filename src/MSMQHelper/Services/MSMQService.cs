﻿using System;
using System.Messaging;
using MSMQHelper.Models;

namespace MSMQHelper.Services
{
    public class MSMQService : IMSMQService
    {
        public bool SendMessage(QueueDefinition queueDefinition, MessageDefinition messageDefinition)
        {
            var queue = new MessageQueue(queueDefinition.QueuePath);
            if (!MessageQueue.Exists(queueDefinition.QueuePath))
            {
                throw new Exception("Queue doesn't exist");
            }

            var message = new Message(messageDefinition.Body, new ActiveXMessageFormatter())
            {
                Recoverable = messageDefinition.IsRecoverable,
                Label = messageDefinition.Label,
                ResponseQueue = queue
            };

            if (queue.Transactional)
            {
                using (var ts = new MessageQueueTransaction())
                {
                    ts.Begin();
                    queue.Send(message, ts);
                    ts.Commit();
                }
            }
            else
            {
                queue.Send(message);
            }

            return true;
        }
    }
}
