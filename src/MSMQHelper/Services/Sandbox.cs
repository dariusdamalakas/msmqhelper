﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MSMQHelper.Services
{
    public class Sandbox : IDisposable
    {
        private readonly AppDomain appDomain;

        private Sandbox(AppDomain appDomain)
        {
            this.appDomain = appDomain;
        }

        public static Sandbox Create(string domainName, string privateBinPath)
        {
            var configFile = Directory.GetFiles(privateBinPath, "*.config").FirstOrDefault();
            
            var setup = new AppDomainSetup
                            {
                                ApplicationBase = Environment.CurrentDirectory,
                                ConfigurationFile = configFile ?? AppDomain.CurrentDomain.SetupInformation.ConfigurationFile,
                                PrivateBinPath = privateBinPath
                            };
            var appDomain = AppDomain.CreateDomain(domainName, AppDomain.CurrentDomain.Evidence, setup);

            return new Sandbox(appDomain);
        }

        public T InjectRemoteObject<T>(object[] args) where T : MarshalByRefObject
        {
            return (T)appDomain.CreateInstanceAndUnwrap(typeof(T).Assembly.FullName, typeof(T).FullName, false, 0, null, args, null, null);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                if (appDomain != null)
                    AppDomain.Unload(appDomain);
        }
    }
}